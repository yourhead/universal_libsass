

all: sass inc
clean: clean_source

intel: compile_intel
arm: compile_arm




#
# Set up derived and product dirs if we want
# to run on the command line instead of in xcode
#
ifndef DERIVED_FILE_DIR
DERIVED_FILE_DIR = derived
endif

ifndef BUILT_PRODUCTS_DIR
BUILT_PRODUCTS_DIR = products
endif

SOURCE_DIR = libsass
UNIVERSAL_MAKEFILE = universalMakefile
DERIVED_INTEL_DIR = $(DERIVED_FILE_DIR)/intel
DERIVED_ARM_DIR = $(DERIVED_FILE_DIR)/arm




ifndef LIBSASS_VERSION
	ifneq ($(wildcard ./.git/ ),)
		LIBSASS_VERSION ?= $(shell cd "$(SOURCE_DIR)"  ;  git describe --abbrev=4 --dirty --always --tags)
	endif
	ifneq ($(wildcard VERSION),)
		LIBSASS_VERSION ?= $(shell $(CAT) VERSION)
	endif
endif


version:
	echo "Version: $(LIBSASS_VERSION)"









TARGET_INTEL = intel64
TARGET_ARM = arm64

CONFIG_INTEL = TARGET=$(TARGET_INTEL) LIBSASS_VERSION=$(LIBSASS_VERSION)
CONFIG_ARM = TARGET=$(TARGET_ARM) LIBSASS_VERSION=$(LIBSASS_VERSION)








clean_intel: clean_derived_intel

clean_arm: clean_derived_arm



clean_setup: clean_derived
clean_config: clean_derived
clean_source: clean_derived
	cd "$(SOURCE_DIR)" ;  make clean


clean_derived: clean_derived_intel clean_derived_arm clean_products
	rm -rf $(DERIVED_FILE_DIR)

clean_derived_intel: clean_products
	rm -rf $(DERIVED_INTEL_DIR)

clean_derived_arm: clean_products
	rm -rf $(DERIVED_ARM_DIR)


clean_products:
	rm -rf $(BUILT_PRODUCTS_DIR)



#
# Create a derived dir for each platform
# copy the source directories entirely.
# this is overkill but ensures nothing is missed
# and allows us to run test, etc.
#
SETUP_INTEL = $(DERIVED_FILE_DIR)/_setup_intel
SETUP_ARM = $(DERIVED_FILE_DIR)/_setup_arm

setup: setup_intel setup_arm

setup_intel: $(SETUP_INTEL)
$(SETUP_INTEL): $(SOURCE_DIR) $(UNIVERSAL_MAKEFILE)
	mkdir -p $(DERIVED_FILE_DIR)
	rm -Rf $(DERIVED_INTEL_DIR)
	cp -Rf $(SOURCE_DIR) $(DERIVED_INTEL_DIR)
	cp -Rf $(UNIVERSAL_MAKEFILE) $(DERIVED_INTEL_DIR)/Makefile
	touch $(SETUP_INTEL)

setup_arm: $(SETUP_ARM)
$(SETUP_ARM): $(SOURCE_DIR) $(UNIVERSAL_MAKEFILE)
	mkdir -p $(DERIVED_FILE_DIR)
	rm -Rf $(DERIVED_ARM_DIR)
	cp -Rf $(SOURCE_DIR) $(DERIVED_ARM_DIR)
	cp -Rf $(UNIVERSAL_MAKEFILE) $(DERIVED_ARM_DIR)/Makefile
	touch $(SETUP_ARM)




#
# do the compile for each platform
#
COMPILED_INTEL = $(DERIVED_FILE_DIR)/_compiled_intel
COMPILED_ARM = $(DERIVED_FILE_DIR)/_compiled_arm

compile: $(COMPILED_ARM) $(COMPILED_INTEL)

compile_intel: $(COMPILED_INTEL)
$(COMPILED_INTEL): $(SETUP_INTEL) makefile
	cd $(DERIVED_INTEL_DIR) ; /usr/bin/make $(CONFIG_INTEL)
	cd $(DERIVED_INTEL_DIR) ; /usr/bin/make $(CONFIG_INTEL)
	touch $(COMPILED_INTEL)

compile_arm: $(COMPILED_ARM)
$(COMPILED_ARM): $(SETUP_ARM) makefile
	cd $(DERIVED_ARM_DIR) ; /usr/bin/make $(CONFIG_ARM)
	cd $(DERIVED_ARM_DIR) ; /usr/bin/make $(CONFIG_ARM)
	touch $(COMPILED_ARM)




sass_intel: $(DERIVED_INTEL_DIR)/lib/libsass.a
$(DERIVED_INTEL_DIR)/lib/libsass.a: $(COMPILED_INTEL)

sass_arm: $(DERIVED_ARM_DIR)/lib/libsass.a
$(DERIVED_ARM_DIR)/lib/libsass.a: $(COMPILED_ARM)

inc_arm: $(DERIVED_ARM_DIR)/include
$(DERIVED_ARM_DIR)/include: $(COMPILED_ARM)





sass: $(BUILT_PRODUCTS_DIR)/libsass.a
$(BUILT_PRODUCTS_DIR)/libsass.a: $(DERIVED_INTEL_DIR)/lib/libsass.a $(DERIVED_ARM_DIR)/lib/libsass.a 
	mkdir -p $(BUILT_PRODUCTS_DIR)
	lipo -create $(DERIVED_ARM_DIR)/lib/libsass.a $(DERIVED_INTEL_DIR)/lib/libsass.a -output $(BUILT_PRODUCTS_DIR)/libsass.a

inc: $(BUILT_PRODUCTS_DIR)/include
$(BUILT_PRODUCTS_DIR)/include: $(DERIVED_ARM_DIR)/include
	mkdir -p $(BUILT_PRODUCTS_DIR)
	rm -rf $(BUILT_PRODUCTS_DIR)/include
	cp -rf $(DERIVED_ARM_DIR)/include $(BUILT_PRODUCTS_DIR)/include
